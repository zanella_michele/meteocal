/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.presentation;

import it.polimi.meteoCal.boundary.UsersFacade;
import it.polimi.meteoCal.entity.Users;
import java.util.Date;
import javax.faces.context.FacesContext;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Michele Zanella <it.polimi.meteoCal>
 */
public class RegistrationViewTest {

    RegistrationView rv = new RegistrationView();

    @Test
    public void testRegister() {
        UsersFacade uf = mock(UsersFacade.class);
        FacesContext fc = mock(FacesContext.class);
        rv.userFacade = uf;
        rv.fc = fc;
        Users newUser1 = new Users();
        newUser1.setEmail("a@a.a");
        newUser1.setName("a");
        newUser1.setSurname("b");
        newUser1.setDateOfBirth(new Date(111));
        newUser1.setType("USERS");
        newUser1.setUsername("asd");
        newUser1.setPassword("asd");
        rv.setUser(newUser1);

        when(uf.isUsernameUnique(newUser1.getUsername())).thenReturn(Boolean.TRUE);
        when(uf.isEmailUnique(newUser1.getEmail())).thenReturn(Boolean.TRUE);

        assertNotNull(rv.register());

        when(uf.isUsernameUnique(newUser1.getUsername())).thenReturn(Boolean.FALSE);

        assertNull(rv.register());

        when(uf.isUsernameUnique(newUser1.getUsername())).thenReturn(Boolean.TRUE);
        when(uf.isEmailUnique(newUser1.getEmail())).thenReturn(Boolean.FALSE);

        assertNull(rv.register());
    }

    @Test
    public void testGetUser() {
        Users user = rv.getUser();
        assertNotNull(user.getCalendar());
        assertTrue(user.getType().equals("USERS"));
    }
}
