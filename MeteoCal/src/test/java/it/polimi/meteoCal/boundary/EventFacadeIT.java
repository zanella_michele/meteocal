/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.boundary;

import it.polimi.meteoCal.control.core.ResetManager;
import it.polimi.meteoCal.entity.Event;
import it.polimi.meteoCal.entity.Request;
import it.polimi.meteoCal.entity.Users;
import it.polimi.meteoCal.entity.primeFaces.ScheduleModelImpl;
import java.security.Principal;
import java.util.Calendar;
import java.util.Date;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Michele Zanella <it.polimi.meteoCal>
 */
@RunWith(Arquillian.class)
public class EventFacadeIT {

    @EJB
    UsersFacade uf;
    @Inject
    private Principal principal;
    @EJB
    private RequestFacade requestFacade;
    @EJB
    private UpdatesFacade updateFacade;
    @EJB
    private BadWeatherNotificationFacade bwnFacade;
    @EJB
    private SuggestionFacade suggestionFacade;

    @EJB
    ResetManager rm;
    @EJB
    EventFacade ef;
    @EJB//
    CalendarFacade cf;//
    @EJB
    private WeatherForecastFacade weatherForecastFacade;

    @PersistenceContext
    EntityManager em;

    Users newUser1 = new Users();
    Users newUser2 = new Users();
    String sameUsername = "asd";
    String email = "asd@asd.it";
    String password = "asd";
    Date birthDate = Calendar.getInstance().getTime();
    String name = "Name";
    String surname = "Surname";

    Event event;

    @Deployment
    public static WebArchive createArchiveAndDeploy() {
        return ShrinkWrap.create(WebArchive.class)
                .addClass(UsersFacade.class)
                .addClass(EventFacade.class)
                .addClass(CalendarFacade.class)//
                .addClass(RequestFacade.class)
                .addClass(UpdatesFacade.class)
                .addClass(BadWeatherNotificationFacade.class)
                .addClass(SuggestionFacade.class)
                .addClass(Principal.class)
                .addClass(ResetManager.class)
                .addClass(WeatherForecastFacade.class)
                .addClass(ScheduleModelImpl.class)//
                .addPackage(Users.class.getPackage())
                .addPackage(Event.class.getPackage())
                .addPackage(Request.class.getPackage())
                .addPackage(Calendar.class.getPackage())//
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Before
    public void setUp() {
        uf.requestFacade = requestFacade;
        uf.principal = principal;
        uf.bwnFacade = bwnFacade;
        uf.suggestionFacade = suggestionFacade;
        uf.updateFacade = updateFacade;
        uf.em = em;
        cf.em = em;

        ef.weatherForecastFacade = weatherForecastFacade;
        ef.requestFacade = requestFacade;
        ef.em = em;
        //Setting newUser1
        newUser1.setEmail(email);
        newUser1.setName(name);
        newUser1.setSurname(surname);
        newUser1.setDateOfBirth(birthDate);
        newUser1.setType("USERS");
        newUser1.setCalendar(new it.polimi.meteoCal.entity.Calendar());
        newUser1.setUsername(sameUsername);
        newUser1.setPassword(password);
        //Setting newUser2
        newUser2.setEmail("dif@dif.it");
        newUser2.setName(name);
        newUser2.setSurname(surname);
        newUser2.setDateOfBirth(birthDate);
        newUser2.setType("USERS");
        newUser2.setCalendar(new it.polimi.meteoCal.entity.Calendar());
        newUser2.setUsername("sad");
        newUser2.setPassword(password);
        //Setting event
        event = new Event("Nome evento", birthDate, birthDate);
        event.setCreatedBy(newUser1);
        event.setPlace("casa");
        event.setAllDay(false);
        event.setEditable(false);
        event.setIsOutdoor(false);
        event.setIsPublic(false);

        //Creating request
        Request req = new Request(event, newUser2);

        rm.em = em;
        rm.resetDB();

        uf.create(newUser1);
        uf.create(newUser2);
        event.addGoingUser(newUser1.getCalendar());//
        newUser1.getCalendar().addEvent(event);//

        ef.create(event);
        cf.edit(newUser1.getCalendar());//

        newUser2.addRequest(req);

        //ef.edit(event);
        uf.edit(newUser2);

    }

    @Test
    public void testGetInvitedUser() {
        assertEquals(ef.getInvitedUser(event).get(0), newUser2);
    }

    @Test
    public void testGetRequired() {
        newUser2.getRequest(event).setIsRead(true);
        uf.edit(newUser2);
        assertEquals(ef.getRequestedUser(event).get(0), newUser2);
    }

    @Test
    public void testGetGoing() {
        newUser2.getCalendar().addEvent(event);
        event.addGoingUser(newUser2.getCalendar());
        newUser2.getRequest(event).setAccepted(true);
        newUser2.getRequest(event).setIsRead(true);
        uf.edit(newUser2);
        ef.edit(event);
        cf.edit(newUser2.getCalendar());
        assertTrue(ef.getGoingUsers(event).contains(newUser2.getCalendar()));
    }

}
