/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.boundary;

import it.polimi.meteoCal.entity.Users;
import javax.persistence.EntityManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 *
 * @author Michele Zanella <it.polimi.meteoCal>
 */
public class UsersFacadeTest {

    private UsersFacade uf;

    @Before
    public void setUp() {
        uf = new UsersFacade();
        uf.em = mock(EntityManager.class);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void newUsersShouldBelongToUserGroupAndSavedOnce() {
        Users newUser = new Users();
        uf.create(newUser);
        verify(uf.em, times(1)).persist(newUser);
    }
}
