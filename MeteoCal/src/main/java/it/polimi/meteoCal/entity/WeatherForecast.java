/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.entity;

import java.io.Serializable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

/**
 *
 * @author Utente
 */
@Entity(name = "WEATHERFORECAST")
@NamedQuery(name = WeatherForecast.removeAllByEvent,
        query = "DELETE FROM WEATHERFORECAST wf WHERE wf.event= :event")
public class WeatherForecast implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private static final long serialVersionUID = 1L;
    public static final String removeAllByEvent = "WeatherForecast.removeAllByEvent";
    @Embedded
    private Weather weather;
    @ManyToOne
    @JoinColumn(name = "EVENT_ID", nullable = false)
    private Event event;

    /**
     * Constructor method
     */
    public WeatherForecast() {
        this.weather = new Weather();
    }

    /**
     *
     * @return the weather forecast's id
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id the weather forecast's id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return the Weather condition retrieved by the system
     */
    public Weather getWeather() {
        return weather;
    }

    /**
     *
     * @param weather the Weather condition retrieved by the system
     */
    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    /**
     *
     * @return the Event associated to this weather forecast
     */
    public Event getEvent() {
        return event;
    }

    /**
     *
     * @param event the Event associated to this weather forecast
     */
    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WeatherForecast)) {
            return false;
        }
        WeatherForecast other = (WeatherForecast) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.polimi.meteoCal.entity.WeatherForecast[ id=" + id + " ]";
    }

}
