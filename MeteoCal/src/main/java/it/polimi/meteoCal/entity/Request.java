/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author Michele
 */
@Entity(name = "REQUEST")
@NamedQueries({
    @NamedQuery(name = Request.findInvited, query = "SELECT r FROM REQUEST r WHERE r.event = :event AND r.accepted=false"),
    @NamedQuery(name = Request.findAllByUser, query = "SELECT r FROM REQUEST r WHERE r.user= :user AND r.isRead=false"),
    @NamedQuery(name = Request.findRequested, query = "SELECT r FROM REQUEST r WHERE r.event = :event")
})
public class Request extends Notification {

    private static final long serialVersionUID = 1L;
    public static final String findInvited = "Request.findInvited";
    public static final String findAllByUser = "Request.findAllByUser";
    public static final String findRequested = "Request.findRequested";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "EVENT_ID", nullable = false)
    private Event event;

    private String invitationMessage;
    private boolean isRead;
    private boolean accepted;

    @ManyToOne
    @JoinColumn(name = "USER_ID", nullable = false)
    private Users user;

    public Request() {
    }

    public Request(Event event, Users user) {
        this.event = event;
        this.user = user;
    }

    /**
     *
     * @return the request's id
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id the request's id
     */
    public void setId(Long id) {
        this.id = id;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public String getInvitationMessage() {
        return invitationMessage;
    }

    public void setInvitationMessage(String invitationMessage) {
        this.invitationMessage = invitationMessage;
    }

    public boolean isIsRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Request)) {
            return false;
        }
        Request other = (Request) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.polimi.meteoCal.entity.Request[ id=" + id + " ]";
    }

}
