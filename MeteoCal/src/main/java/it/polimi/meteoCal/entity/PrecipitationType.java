/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.entity;

/**
 *
 * @author Michele
 */
public enum PrecipitationType {

    Clear("Clear"),
    Clouds("Clouds"),
    Rain("Rain"),
    Snow("Snow");

    private final String description;

    public String getDescription() {
        return description;
    }

    private PrecipitationType(String description) {
        this.description = description;
    }
;
}
