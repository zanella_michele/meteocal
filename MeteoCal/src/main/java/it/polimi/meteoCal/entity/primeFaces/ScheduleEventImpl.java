/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.entity.primeFaces;

import it.polimi.meteoCal.entity.Event;
import java.io.Serializable;
import java.util.Date;
import org.primefaces.model.ScheduleEvent;

/**
 *
 * @author Michele
 */
public class ScheduleEventImpl implements ScheduleEvent, Serializable {

    private Event event;

    private String id;
    private boolean allDay = false;
    private String styleClass;
    private Object data;

    public ScheduleEventImpl() {
        event = new Event();
    }

    public ScheduleEventImpl(String title, Date start, Date end) {
        this();
        event.setTitle(title);
        event.setStartDate(start);
        event.setEndDate(end);
    }

    public ScheduleEventImpl(String title, Date start, Date end, boolean allDay) {
        this();
        event.setTitle(title);
        event.setStartDate(start);
        event.setEndDate(end);
        this.allDay = allDay;
    }

    public ScheduleEventImpl(String title, Date start, Date end, String styleClass) {
        this();
        event.setTitle(title);
        event.setStartDate(start);
        event.setEndDate(end);
        this.styleClass = styleClass;
    }

    public ScheduleEventImpl(String title, Date start, Date end, Object data) {
        this();
        event.setTitle(title);
        event.setStartDate(start);
        event.setEndDate(end);
        this.data = data;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getTitle() {
        return event.getTitle();
    }

    public void setTitle(String title) {
        event.setTitle(title);
    }

    @Override
    public Date getStartDate() {
        return event.getStartDate();
    }

    public void setStartDate(Date startDate) {
        event.setStartDate(startDate);
    }

    @Override
    public Date getEndDate() {
        return event.getEndDate();
    }

    public void setEndDate(Date endDate) {
        event.setEndDate(endDate);
    }

    @Override
    public boolean isAllDay() {
        return allDay;
    }

    public void setAllDay(boolean allDay) {
        this.allDay = allDay;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    @Override
    public String getStyleClass() {
        return styleClass;
    }

    @Override
    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public boolean isEditable() {
        return false;
//return event.isPublic();
    }

    public void setEditable(boolean editable) {
        event.setIsPublic(editable);
    }

    @Override
    public String getDescription() {
        return event.getDescription();
    }

    public void setDescription(String description) {
        event.setDescription(description);
    }

    @Override

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ScheduleEventImpl other = (ScheduleEventImpl) obj;
        if ((this.getTitle() == null) ? (other.getTitle() != null) : !this.getTitle().equals(other.getTitle())) {
            return false;
        }
        if (this.getStartDate() != other.getStartDate() && (this.getStartDate() == null || !this.getStartDate().equals(other.getStartDate()))) {
            return false;
        }
        if (this.getEndDate() != other.getEndDate() && (this.getEndDate() == null || !this.getEndDate().equals(other.getEndDate()))) {
            return false;
        }
        return true;
    }

    @Override

    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + (this.getTitle() != null ? this.getTitle().hashCode() : 0);
        hash = 61 * hash + (this.getStartDate() != null ? this.getStartDate().hashCode() : 0);
        hash = 61 * hash + (this.getEndDate() != null ? this.getEndDate().hashCode() : 0);
        return hash;
    }

    @Override

    public String toString() {
        return "DefaultScheduleEvent{title=" + getTitle() + ",startDate=" + getStartDate() + ",endDate=" + getEndDate() + "}";
    }

}
