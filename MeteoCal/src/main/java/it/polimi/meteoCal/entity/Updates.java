/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

/**
 *
 * @author Michele
 */
@Entity(name = "UPDATES")
@NamedQuery(name = Updates.findAllByUser, query = "SELECT up FROM UPDATES up WHERE up.user= :user AND up.isRead=false")
public class Updates extends Notification {

    private static final long serialVersionUID = 1L;
    public static final String findAllByUser = "Updates.findAllByUser";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String message;

    private boolean isRead;

    @ManyToOne()
    @JoinColumn(name = "EVENT_ID", nullable = false)
    private Event event;

    @ManyToOne
    @JoinColumn(name = "USER_ID", nullable = false)
    private Users user;

    public Updates() {
    }

    public Updates(Event event, Users user) {
        this.event = event;
        this.user = user;
    }

    /**
     *
     * @return the update's id
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id the update's id
     */
    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isIsRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    /**
     *
     * @return the associated event which weather notification refers to
     */
    public Event getEvent() {
        return event;
    }

    /**
     *
     * @param event: the associated event which weather notification refers to
     */
    public void setEvent(Event event) {
        this.event = event;
    }

    /**
     *
     * @return a list of all usersList that received this notification
     */
    public Users getUsersList() {
        return user;
    }

    /**
     *
     * @param usersList: a list of all usersList that received this notification
     */
    public void setUsersList(Users usersList) {
        this.user = usersList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Updates)) {
            return false;
        }
        Updates other = (Updates) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.polimi.meteoCal.entity.Update[ id=" + id + " ]";
    }

}
