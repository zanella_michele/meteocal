/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.entity.primeFaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

/**
 *
 * @author Michele
 */
@SessionScoped
public class ScheduleModelImpl implements ScheduleModel, Serializable {

    private List<ScheduleEvent> events;

    /**
     * Constructor method, creates a new events arraylist
     */
    public ScheduleModelImpl() {
        events = new ArrayList<>();
    }

    /**
     * Adds an event to the going events list
     *
     * @param event the event to be added
     */
    @Override
    public void addEvent(ScheduleEvent event) {
        events.add(event);
    }

    /**
     * Removes an event from the going events list
     *
     * @param event the event to be removed
     * @return true if the remove action is correctly performed, false otherwise
     */
    @Override
    public boolean deleteEvent(ScheduleEvent event) {
        return events.remove(event);
    }

    /**
     *
     * @return a list with all user's going events
     */
    @Override
    public List<ScheduleEvent> getEvents() {
        return events;
    }

    /**
     * Gets a specific event
     *
     * @param id the event's id to be retrieved
     * @return the event searched for, null if it doesn't exists
     */
    @Override
    public ScheduleEvent getEvent(String id) {
        for (ScheduleEvent event : events) {
            if (event.getId().equals(id)) {
                return event;
            }
        }

        return null;
    }

    /**
     * Updates a specific event
     *
     * @param event the event to be updates
     */
    @Override
    public void updateEvent(ScheduleEvent event) {
        int index = -1;

        for (int i = 0; i < events.size(); i++) {
            if (events.get(i).getId().equals(event.getId())) {
                index = i;
                break;
            }
        }
        if (index >= 0) {
            events.set(index, event);
        }
    }

    /**
     *
     * @return the number of going events
     */
    @Override
    public int getEventCount() {
        return events.size();
    }

    /**
     * Removes all going events
     */
    @Override
    public void clear() {
        events = new ArrayList<>();
    }

}
