/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.entity;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 *
 * @author Michele
 */
@Embeddable
public class Weather implements Serializable {

    private static final long serialVersionUID = 1L;

    private PrecipitationType precipitationType;
    private int windSpeed;
    private int temperature;
    private int cloudsCoverage;

    public boolean boolPrecipitationType;
    public boolean boolwindSpeed;
    public boolean boolTemperature;
    public boolean boolCloudsCoverage;

    /**
     *
     * @return the type of the precipitation
     */
    public PrecipitationType getPrecipitationType() {
        return precipitationType;
    }

    /**
     *
     * @param precipitationType the type of the precipitation
     */
    public void setPrecipitationType(PrecipitationType precipitationType) {
        this.precipitationType = precipitationType;
    }

    /**
     *
     * @return the wind speed
     */
    public int getWindSpeed() {
        return windSpeed;
    }

    /**
     *
     * @param windSpeed the wind speed
     */
    public void setWindSpeed(int windSpeed) {
        this.windSpeed = windSpeed;
    }

    /**
     *
     * @return the temperature
     */
    public int getTemperature() {
        return temperature;
    }

    /**
     *
     * @param temperature the temperature
     */
    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    /**
     *
     * @return the clouds coverage
     */
    public int getCloudsCoverage() {
        return cloudsCoverage;
    }

    /**
     *
     * @param cloudsCoverage the clouds coverage
     */
    public void setCloudsCoverage(int cloudsCoverage) {
        this.cloudsCoverage = cloudsCoverage;
    }

    public boolean isBoolPrecipitationType() {
        return boolPrecipitationType;
    }

    public void setBoolPrecipitationType(boolean boolPrecipitationType) {
        this.boolPrecipitationType = boolPrecipitationType;
    }

    public boolean isBoolwindSpeed() {
        return boolwindSpeed;
    }

    public void setBoolwindSpeed(boolean boolwindSpeed) {
        this.boolwindSpeed = boolwindSpeed;
    }

    public boolean isBoolTemperature() {
        return boolTemperature;
    }

    public void setBoolTemperature(boolean boolTemperature) {
        this.boolTemperature = boolTemperature;
    }

    public boolean isBoolCloudsCoverage() {
        return boolCloudsCoverage;
    }

    public void setBoolCloudsCoverage(boolean boolCloudsCoverage) {
        this.boolCloudsCoverage = boolCloudsCoverage;
    }
    /*
     @Override
     public int hashCode() {
     int hash = 0;
     hash += (id != null ? id.hashCode() : 0);
     return hash;
     }
     */

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Weather)) {
            return false;
        }
        Weather other = (Weather) object;
        if ((this.precipitationType.equals(other.precipitationType))
                && (this.temperature == other.temperature)
                && (this.cloudsCoverage == other.cloudsCoverage)
                && (this.windSpeed == other.windSpeed)) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param other the object to compare
     * @return true if the weather is isWorst than the given weather
     */
    public boolean isWorst(Weather other) {
        if (!(other instanceof Weather)) {
            return false;
        }
        if ((other.isBoolPrecipitationType() && (this.precipitationType.compareTo(other.precipitationType) > 0))
                || (other.isBoolTemperature() && (this.temperature <= other.temperature))
                || (other.isBoolCloudsCoverage() && (this.cloudsCoverage >= other.cloudsCoverage))
                || (other.isBoolwindSpeed() && (this.windSpeed >= other.windSpeed))) {
            return true;
        }
        return false;

    }
    /*
     @Override
     public String toString() {
     return "it.polimi.meteoCal.entity.Weather[ id=" + id + " ]";
     }


     */
}
