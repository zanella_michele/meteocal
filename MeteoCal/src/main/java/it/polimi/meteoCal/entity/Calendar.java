/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Michele
 */
@Entity(name = "CALENDAR")
public class Calendar implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(optional = false, mappedBy = "calendar", cascade = CascadeType.ALL)
    private Users user;

    @NotNull
    private Boolean shared;

    @ManyToMany(mappedBy = "goingUsers", fetch = FetchType.EAGER)
    private List<Event> goingEvents;

    /**
     * Constructor method
     */
    public Calendar() {
        this.shared = false;
        goingEvents = new ArrayList<>();
    }

    public Boolean getShared() {
        return shared;
    }

    public void setShared() {
        this.shared = !shared;
    }

    /**
     *
     * @return the id of the user's calendar
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id: the id of the user's calendar
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return the user associated to the calendar
     */
    public Users getUser() {
        return user;
    }

    /**
     *
     * @param user: the user associated to the calendar
     */
    public void setUser(Users user) {
        this.user = user;
    }

    /**
     *
     * @return a list with all the events in which the user takes part
     */
    public List<Event> getGoingEvents() {
        if (goingEvents == null) {
            goingEvents = new ArrayList<>();
        }
        return goingEvents;
    }

    /**
     *
     * @param goingEvents:a list with all the events in which the user takes
     * part
     */
    public void setGoingEvents(List<Event> goingEvents) {
        this.goingEvents = goingEvents;
    }

    /**
     * Method to add an event to the user's calendar
     *
     * @param event: the event to add
     */
    public void addEvent(Event event) {
        goingEvents.add(event);
        if (!event.getGoingUsers().contains(this)) {
            event.addGoingUser(this);
        }
    }

    /**
     * Method to remove an event from the user's calendar
     *
     * @param event: the event to remove
     */
    public void removeEvent(Event event) {
        goingEvents.remove(event);
        if (event.getGoingUsers().contains(this)) {
            event.removeGoingUser(this);
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Calendar)) {
            return false;
        }
        Calendar other = (Calendar) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.polimi.meteoCal.entity.Calendar[ id=" + id + " ]";
    }

}
