/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author Michele
 */
@Entity(name = "BADWEATHER")
public class BadWeather implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Embedded
    private Weather weather;
    @OneToOne(optional = false, cascade = CascadeType.ALL)
    private Event event;

    /**
     * Constructor method
     */
    public BadWeather() {
        this.weather = new Weather();
    }

    /**
     *
     * @return the id of the BadWeather specification
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id: the id for the bad weather sepcification
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return the Weather condition specified by the user as bad weather
     */
    public Weather getWeather() {
        return weather;
    }

    /**
     *
     * @param weather: the Weather condition specified by the user as bad
     * weather
     */
    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    /**
     *
     * @return the Event associated to this bad weather specfication
     */
    public Event getEvent() {
        return event;
    }

    /**
     *
     * @param event: the Event associated to this bad weather specfication
     */
    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BadWeather)) {
            return false;
        }
        BadWeather other = (BadWeather) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.polimi.meteoCal.entity.BadWeather[ id=" + id + " ]";
    }

}
