/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

/**
 *
 * @author Michele
 */
@Entity(name = "BADWEATHERNOTIFICATION")
@NamedQuery(name = BadWeatherNotification.findAllByUser, query = "SELECT bwn FROM BADWEATHERNOTIFICATION bwn WHERE bwn.user= :user AND bwn.isRead=false")
public class BadWeatherNotification extends Notification {

    private static final long serialVersionUID = 1L;
    public static final String findAllByUser = "BadWeatherNotification.findAllByUser";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private boolean isRead;

    @ManyToOne()
    @JoinColumn(name = "EVENT_ID", nullable = false)
    private Event event;

    @ManyToOne
    @JoinColumn(name = "USER_ID", nullable = false)
    private Users user;

    public BadWeatherNotification() {
    }

    public BadWeatherNotification(Event event, Users user) {
        this.event = event;
        this.user = user;
    }

    /**
     *
     * @return the id of the weather notification
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id: the id of the weather notification
     */
    public void setId(Long id) {
        this.id = id;
    }

    public boolean isIsRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    /**
     *
     * @return the associated event which weather notification refers to
     */
    public Event getEvent() {
        return event;
    }

    /**
     *
     * @param event: the associated event which weather notification refers to
     */
    public void setEvent(Event event) {
        this.event = event;
    }

    /**
     *
     * @return a list of all usersList that received this notification
     */
    public Users getUsersList() {
        return user;
    }

    /**
     *
     * @param usersList: a list of all usersList that received this notification
     */
    public void setUsersList(Users usersList) {
        this.user = usersList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BadWeatherNotification)) {
            return false;
        }
        BadWeatherNotification other = (BadWeatherNotification) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.polimi.meteoCal.entity.BadWeatherNotification[ id=" + id + " ]";
    }

}
