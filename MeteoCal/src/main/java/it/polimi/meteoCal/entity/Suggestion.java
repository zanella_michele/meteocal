/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Michele
 */
@Entity(name = "SUGGESTION")
@NamedQuery(name = Suggestion.findAllByUser, query = "SELECT s FROM SUGGESTION s WHERE s.user= :user AND s.isRead=false")
public class Suggestion extends Notification {

    private static final long serialVersionUID = 1L;
    public static final String findAllByUser = "Suggestion.findAllByUser";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne()
    @JoinColumn(name = "EVENT_ID", nullable = false)
    private Event event;

    @ManyToOne
    @JoinColumn(name = "USER_ID", nullable = false)
    private Users user;

    private boolean accepted;
    private boolean isRead;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    public Suggestion() {

    }

    public Suggestion(Event event, Users user, Date startDate, Date endDate) {
        this.event = event;
        this.user = user;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    /**
     *
     * @return the suggestion's id
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id the suggestions' id
     */
    public void setId(Long id) {
        this.id = id;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public boolean isIsRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Suggestion)) {
            return false;
        }
        Suggestion other = (Suggestion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.polimi.meteoCal.entity.Suggestion[ id=" + id + " ]";
    }

}
