/*
 * Copyright (C) 2015 Michele Zanella <it.polimi.meteoCal>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.polimi.meteoCal.presentation;

import it.polimi.meteoCal.entity.Event;
import it.polimi.meteoCal.entity.primeFaces.ScheduleModelImpl;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

/**
 *
 * @author Michele Zanella <it.polimi.meteoCal>
 */
@Named(value = "otherCalendarView")
@SessionScoped
public class OtherCalendarView implements Serializable {

    @Inject
    private EventView eventView;
    @Inject
    private UsersView usersView;

    private ScheduleModel eventModel;

    private Event event = new Event();

    /**
     * This method is executed after the construction and sets some critical
     * fields used by the PrimeFaces libraries
     */
    @PostConstruct
    public void init() {
        eventModel = new ScheduleModelImpl();
        List<Event> eventsList;
        eventsList = usersView.getUser().getCalendar().getGoingEvents();
        for (Event e : eventsList) {
            Event ev = new Event();
            if (e.getIsPublic()) {
                eventModel.addEvent(e);

            } else {
                ev.setId(e.getId());
                ev.setTitle("Private event");
                ev.setStartDate(e.getStartDate());
                ev.setEndDate(e.getEndDate());
                ev.setAllDay(e.isAllDay());
                ev.setPlace("Private event");
                ev.setIsOutdoor(false);
                ev.setIsPublic(false);
                ev.setEditable(false);
                eventModel.addEvent(ev);
            }
        }
    }

    /**
     *
     * @return the initial date for the calendar
     */
    public Date getInitialDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), Calendar.FEBRUARY, calendar.get(Calendar.DATE), 0, 0, 0);

        return calendar.getTime();
    }

    /**
     *
     * @return the PrimeFaces event model
     */
    public ScheduleModel getEventModel() {
        return eventModel;
    }

    /**
     *
     * @return a specific temp event
     */
    public Event getEvent() {
        return event;
    }

    /**
     *
     * @param event a specific temp event
     */
    public void setEvent(Event event) {
        this.event = event;
    }

    /**
     * Add or update an event in the eventModel
     *
     * @param actionEvent an action event performed by the user
     */
    /*    public void addEvent(ActionEvent actionEvent) {
     if (event.getId() == null) {
     eventModel.addEvent(event);
     } else {
     eventModel.updateEvent(event);
     }

     event = new Event();
     }*/
    /**
     * Actions perfomed when an user select an event
     *
     * @param selectEvent the selection event
     * @return
     */
    public void onEventSelect(SelectEvent selectEvent) {
        event = (Event) selectEvent.getObject();
        eventView.setEvent((Event) event);
    }

    /**
     * Loads the event's page
     *
     * @return event's page specific from user's type
     */
    public String loadEventPage() {

        return "/users/event.xhtml?faces-redirect=true";
    }
}
