/*
 * Copyright (C) 2015 Michele Zanella <it.polimi.meteoCal>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.polimi.meteoCal.presentation;

import it.polimi.meteoCal.boundary.BadWeatherNotificationFacade;
import it.polimi.meteoCal.boundary.RequestFacade;
import it.polimi.meteoCal.boundary.SuggestionFacade;
import it.polimi.meteoCal.boundary.UpdatesFacade;
import it.polimi.meteoCal.boundary.UsersFacade;
import it.polimi.meteoCal.entity.BadWeatherNotification;
import it.polimi.meteoCal.entity.Request;
import it.polimi.meteoCal.entity.Suggestion;
import it.polimi.meteoCal.entity.Updates;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Utente
 */
@Named(value = "notificationView")
@ViewScoped
public class NotificationView {

    @EJB
    private UsersFacade usersFacade;
    @EJB
    private RequestFacade requestFacade;
    @EJB
    private BadWeatherNotificationFacade badWeatherNotificationFacade;
    @EJB
    private UpdatesFacade updatesFacade;
    @EJB
    private SuggestionFacade suggestionFacade;

    @Inject
    private UsersView usersView;

    private List<Request> requestsList;
    private List<Suggestion> suggestionsList;
    private List<Updates> updatesList;
    private List<BadWeatherNotification> badWeatherList;

    public List<Request> getRequestsList() {
        return requestFacade.findAllByUser(usersView.getLoggedUser());
    }

    public void setRequestsList(List<Request> requestsList) {
        this.requestsList = requestsList;
    }

    public List<Suggestion> getSuggestionsList() {
        return suggestionFacade.findAllByUser(usersView.getLoggedUser());
    }

    public void setSuggestionsList(List<Suggestion> suggestionsList) {
        this.suggestionsList = suggestionsList;
    }

    public List<Updates> getUpdatesList() {
        return updatesFacade.findAllByUser(usersView.getLoggedUser());
    }

    public void setUpdatesList(List<Updates> updatesList) {
        this.updatesList = updatesList;
    }

    public List<BadWeatherNotification> getBadWeatherList() {
        return badWeatherNotificationFacade.findAllByUser(usersView.getLoggedUser());
    }

    public void setBadWeatherList(List<BadWeatherNotification> badWeatherList) {
        this.badWeatherList = badWeatherList;
    }

}
