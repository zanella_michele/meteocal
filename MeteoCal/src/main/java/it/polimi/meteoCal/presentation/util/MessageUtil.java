/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.presentation.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Utente
 */
public class MessageUtil {

    public static void addMessage(String clientId, String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(clientId, message);
        context.getExternalContext().getFlash().setKeepMessages(true);
    }
}
