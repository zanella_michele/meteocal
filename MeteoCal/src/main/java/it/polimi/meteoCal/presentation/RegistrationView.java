/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.presentation;

import it.polimi.meteoCal.boundary.UsersFacade;
import it.polimi.meteoCal.entity.Calendar;
import it.polimi.meteoCal.entity.Users;
import java.text.SimpleDateFormat;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Michele
 */
@Named(value = "registrationView")
@RequestScoped
public class RegistrationView {

    @EJB
    UsersFacade userFacade;
    FacesContext fc;

    private Users user;
    private Calendar calendar;

    public RegistrationView() {
    }

    /**
     *
     * @return the temp user or if it's null it creates a new one with a own
     * calendar
     */
    public Users getUser() {
        if (user == null) {
            user = new Users();
            user.setType("USERS");

            if (calendar == null) {
                calendar = new Calendar();
            }
            calendar.setUser(user);

            user.setCalendar(calendar);
        }
        return user;
    }

    /**
     *
     * @param user the temp user
     */
    public void setUser(Users user) {
        this.user = user;
    }

    /**
     * Action performed when an user selects a date
     *
     * @param event selection event
     */
    public void onDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));
    }

    /**
     * Performs the registration, checking if the username or the email already
     * exists
     *
     * @return null if an error occurs, the MeteoCal index page if the
     * registration succeded
     */
    public String register() {
        if (!userFacade.isUsernameUnique(user.getUsername())) {
            FacesMessage message = new FacesMessage("Username already exists");
            FacesContext context = FacesContext.getCurrentInstance();
            if (context == null) {
                context = fc;
            }
            context.addMessage("registrationform:username", message);
            return null;

        }
        if (!userFacade.isEmailUnique(user.getEmail())) {
            FacesMessage message = new FacesMessage("Email already exists");

            FacesContext context = FacesContext.getCurrentInstance();
            if (context == null) {
                context = fc;
            }
            context.addMessage("registrationform:email", message);
            return null;
        }
        userFacade.create(user);
        return "/index?faces-redirect=true";
    }
}
