/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.presentation;

import it.polimi.meteoCal.boundary.CalendarFacade;
import it.polimi.meteoCal.entity.Event;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

/**
 *
 * @author Michele
 */
@Named(value = "calendarView")
@SessionScoped
public class CalendarView implements Serializable {

    @Inject
    private EventView eventView;
    @EJB
    private CalendarFacade calendarFacade;
    @Inject
    private UsersView usersView;

    private ScheduleModel eventModel;

    private ScheduleEvent event = new Event();

    /**
     * This method is executed after the construction and sets some critical
     * fields used by the PrimeFaces libraries
     */
    @PostConstruct
    public void init() {
        eventModel = calendarFacade.getEventModel();
        List<Event> eventsList;
        eventModel.clear();
        usersView.getLoggedUser().getCalendar().setGoingEvents(calendarFacade.getGoingEvents(usersView.getLoggedUser().getCalendar()));
        eventsList = usersView.getLoggedUser().getCalendar().getGoingEvents();
        for (ScheduleEvent e : eventsList) {
            eventModel.addEvent(e);
        }
    }

    /**
     *
     * @return the initial date for the calendar
     */
    public Date getInitialDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), Calendar.FEBRUARY, calendar.get(Calendar.DATE), 0, 0, 0);

        return calendar.getTime();
    }

    /**
     *
     * @return the PrimeFaces event model
     */
    public ScheduleModel getEventModel() {
        return eventModel;
    }

    /**
     *
     * @return a specific temp event
     */
    public ScheduleEvent getEvent() {
        return event;
    }

    /**
     *
     * @param event a specific temp event
     */
    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }

    /**
     * Add or update an event in the eventModel
     *
     * @param actionEvent an action event performed by the user
     */
    /*    public void addEvent(ActionEvent actionEvent) {
     if (event.getId() == null) {
     eventModel.addEvent(event);
     } else {
     eventModel.updateEvent(event);
     }

     event = new Event();
     }*/
    /**
     * Actions perfomed when an user select an event
     *
     * @param selectEvent the selection event
     * @return
     */
    public void onEventSelect(SelectEvent selectEvent) {
        event = (Event) selectEvent.getObject();
        eventView.setEvent((Event) event);
        //System.out.println(eventView.getEvent().toString());
    }

    /**
     * Actions perfomed when an user select an empty date
     *
     * @param selectEvent the selection event
     * @return start the new event wizard
     */
    public void onDateSelect(SelectEvent selectEvent) {
        event = new Event();
        eventView.setEvent((Event) event);
        eventView.getEvent().setStartDate((Date) selectEvent.getObject());
        eventView.getEvent().setEndDate((Date) selectEvent.getObject());
        //System.out.println(eventView.getEvent().getStartDate() + " " + eventView.getEvent().getEndDate());
    }

    /**
     * Loads the event's page
     *
     * @return event's page specific from user's type
     */
    public String loadEventPage() {
        if (eventView.getEvent().getCreatedBy().equals(usersView.getLoggedUser())) {
            return "/users/event.xhtml?faces-redirect=true&creator=true";
        } else {
            return "/users/event.xhtml?faces-redirect=true&creator=false";
        }
    }

    /**
     * Loads the event wizard start page
     *
     * @return the event wizard start page
     */
    public String startWizard() {
        return "/users/eventWizard.xhtml?faces-redirect=true";
    }
    /* THESE COULD BE USEFULL LATER
     public void onEventMove(ScheduleEntryMoveEvent event) {
     FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event moved", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());

     addMessage(message);
     }

     public void onEventResize(ScheduleEntryResizeEvent event) {
     FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event resized", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());

     addMessage(message);
     }
     

     private void addMessage(FacesMessage message) {
     FacesContext.getCurrentInstance().addMessage(null, message);
     }
     */
}
