/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.presentation;

import it.polimi.meteoCal.boundary.UsersFacade;
import it.polimi.meteoCal.entity.Users;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Andrea
 */
@Named(value = "searchView")
@SessionScoped
public class SearchView implements Serializable {

    @EJB
    private UsersFacade usersFacade;
    @Inject
    private UsersView usersView;

    private List<Users> searchable;

    private List<Users> searched;

    public List<Users> getSearchable() {
        searchable = usersFacade.findAll();
        return searchable;
    }

    public void setSearchable(List<Users> searchable) {
        this.searchable = searchable;
    }

    public List<Users> getSearched() {
        return searched;
    }

    public void setSearched(List<String> searched) {
        for (Users u : this.searchable) {
            if (searched.get(0).equals(u.getUsername())) {
                usersView.setUser(u);
            }
        }
    }

    public String goUserProfile() {
        if (usersView.getUser() == null) {
            return null;
        }
        return "/users/otherUser.xhtml?faces-redirect=true";

    }
}
