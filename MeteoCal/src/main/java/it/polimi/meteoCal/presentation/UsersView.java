/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.presentation;

import it.polimi.meteoCal.boundary.UsersFacade;
import it.polimi.meteoCal.entity.Users;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Utente
 */
@Named(value = "usersView")
@SessionScoped
public class UsersView implements Serializable {

    @EJB
    UsersFacade userFacade;

    private Users loggedUser;

    private Users user;

    public Users getUser() {
        if (user == null) {
            user = new Users();
            user.setUsername("");
        }
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    /**
     *
     * @return the current session's user, retrieving it if it's null
     */
    public Users getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(Users user) {
        this.loggedUser = user;
    }

    /**
     * Update user's information
     *
     * @return redirection to user's profile page
     */
    public String update() {
        userFacade.edit(loggedUser);
        return "/users/profile.xhtml?faces-redirect=true";
    }

    public int userHasNotifications() {
        return userFacade.countNotifications(this.loggedUser);
    }
}
