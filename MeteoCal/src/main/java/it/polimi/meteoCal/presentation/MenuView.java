/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.presentation;

import it.polimi.meteoCal.boundary.CalendarFacade;
import it.polimi.meteoCal.boundary.UsersFacade;
import it.polimi.meteoCal.entity.Calendar;
import it.polimi.meteoCal.presentation.util.MessageUtil;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Utente
 */
@Named("menuView")
@SessionScoped
public class MenuView implements Serializable {

    @EJB
    private CalendarFacade calendarFacade;
    @EJB
    private UsersFacade usersFacade;

    private Calendar calendar;

    public void shareCalendar() {
        calendar = usersFacade.getLoggedUser().getCalendar();
        calendar.setShared();
        calendarFacade.edit(calendar);
        if (calendar.getShared()) {
            MessageUtil.addMessage(null, "Calendar shared");
        } else {
            MessageUtil.addMessage(null, "Calendar unshared");
        }
    }
}
