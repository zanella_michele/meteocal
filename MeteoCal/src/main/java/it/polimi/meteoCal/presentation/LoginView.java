/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.presentation;

import com.sun.enterprise.security.auth.login.common.LoginException;
import it.polimi.meteoCal.boundary.UsersFacade;
import it.polimi.meteoCal.presentation.util.MessageUtil;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Michele
 */
@Named(value = "loginView")
@RequestScoped
public class LoginView {

    @Inject
    private EventView eventView;
    @Inject
    private UsersView usersView;
    @EJB
    private UsersFacade usersFacade;

    private String username;
    private String password;

    private boolean failed = false;

    public boolean isFailed() {
        return failed;
    }

    public void setFailed(boolean failed) {
        this.failed = failed;
    }

    public LoginView() {
    }

    /**
     *
     * @return the temp user's username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     *
     * @param username the temp user's username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return the temp user's password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     *
     * @param password the temp user's username
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Performs the login action using JDBC realm
     *
     * @return the user's home if login succeded, null otherwise
     */
    public String login() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            request.login(this.username, this.password);
            usersView.setLoggedUser(usersFacade.find(this.username));
            if (usersView.userHasNotifications() > 0) {
                MessageUtil.addMessage(null, "You have " + usersView.userHasNotifications() + " notifications not read yet!");
            }
            return "/users/home.xhtml?faces-redirect=true";
        } catch (ServletException e) {
            failed = true;
            context.addMessage("loginform:submit", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login Failed", "Login Failed"));
            return null;
        } catch (LoginException e) {
            failed = true;
            context.addMessage("loginform:submit", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login Failed", "Login Failed"));
            return null;
        }
    }

    /**
     * Performs the logout invalidating the session
     *
     * @return MeteoCal index page
     */
    public String logout() {
        FacesContext context = FacesContext.getCurrentInstance();
        //HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        //request.getSession().invalidate();
        usersView.setLoggedUser(null);
        context.getExternalContext().invalidateSession();
        return "/index.xhtml?faces-redirect=true";
    }

}
