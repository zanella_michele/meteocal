/*
 * Copyright (C) 2015 Michele Zanella <it.polimi.meteoCal>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.polimi.meteoCal.control.core;

import it.polimi.meteoCal.boundary.EventFacade;
import it.polimi.meteoCal.boundary.WeatherForecastFacade;
import it.polimi.meteoCal.control.NotificationManager;
import it.polimi.meteoCal.control.WeatherManager;
import it.polimi.meteoCal.control.util.DateUtil;
import it.polimi.meteoCal.entity.Event;
import it.polimi.meteoCal.entity.WeatherForecast;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;

/**
 *
 * @author Michele
 */
//@Startup
@Singleton
public class CalendarManager {

    @EJB
    private EventFacade eventFacade;
    @EJB
    private NotificationManager notificationManager;
    @EJB
    private WeatherForecastFacade weatherForecastFacade;

    @Schedule(minute = "*/10", hour = "*", persistent = false)
    public void startManager() {
        checkEventsWeather();
        // System.out.println("Sto girando ogni minuto " + System.currentTimeMillis());
    }

    private void checkEventsWeather() {
        List<Event> eventsList = new ArrayList<>();
        eventsList = eventFacade.findAll();
        for (Event e : eventsList) {
            if (DateUtil.isTomorrow(e.getStartDate()) && wantForecast(e)) {
                if (WeatherManager.verifyBadWeatherConditions(e)) {
                    //System.out.println("Brutto tempo per " + e.getTitle() + " del: " + e.getStartDate());
                    notificationManager.createBadWeatherNotification(e);
                }
            } else if (DateUtil.isThreeDaysBefore(e.getStartDate()) && wantForecast(e)) {
                updateWeatherForecast(e);
                if (WeatherManager.verifyBadWeatherConditions(e)) {
                    //System.out.println("SUGGESTION SENT");
                    Date suggestedDate = WeatherManager.findGoodWeatherDate(e);
                    if (suggestedDate != null) {
                        notificationManager.createSuggestionForCreator(e, suggestedDate);
                    }
                }
            }
        }
    }

    private boolean wantForecast(Event e) {
        return (e.isIsOutdoor() && !e.getWeatherSpescs().equals("off"));
    }

    private void updateWeatherForecast(Event e) {
        weatherForecastFacade.removeAllByEvent(e);
        e.setWeatherForecastList(WeatherManager.retrieveForecast(e.getPlace(), e.getStartDate(), e.getEndDate()));
        for (WeatherForecast f : e.getWeatherForecastList()) {
            f.setEvent(e);
        }
        eventFacade.edit(e);
    }
}
