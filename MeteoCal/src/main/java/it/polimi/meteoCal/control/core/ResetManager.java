/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.control.core;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Michele Zanella <it.polimi.meteoCal>
 */
@Stateless
public class ResetManager {

    @PersistenceContext
    public EntityManager em;

    public void resetDB() {
        em.createQuery("DELETE FROM UPDATES up").executeUpdate();
        em.createQuery("DELETE FROM SUGGESTION s").executeUpdate();
        em.createQuery("DELETE FROM REQUEST r").executeUpdate();
        em.createQuery("DELETE FROM BADWEATHERNOTIFICATION bwn").executeUpdate();
        em.createQuery("DELETE FROM WEATHERFORECAST wf").executeUpdate();
        em.createQuery("DELETE FROM EVENT e").executeUpdate();
        em.createQuery("DELETE FROM BADWEATHER bw").executeUpdate();
        em.createQuery("DELETE FROM USERS u").executeUpdate();
        em.createQuery("DELETE FROM CALENDAR c").executeUpdate();
    }

}
