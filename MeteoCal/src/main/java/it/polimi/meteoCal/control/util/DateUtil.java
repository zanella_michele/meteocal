/*
 * Copyright (C) 2015 Michele Zanella <it.polimi.meteoCal>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.polimi.meteoCal.control.util;

import it.polimi.meteoCal.control.WeatherManager;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Michele Zanella <it.polimi.meteoCal>
 */
public class DateUtil {

    /**
     *
     * @return a Calendar object with today's parameters
     */
    public static Calendar today() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));

        return calendar;
    }

    /**
     *
     * @param firstDate
     * @param secondDate
     * @return true if the firstDate (not considering Time) is before the
     * secondDate
     */
    public static boolean isBeforeCalendar(Date firstDate, Date secondDate) {
        Calendar firstDateCal = Calendar.getInstance();
        firstDateCal.setTime(firstDate);
        Calendar secondDateCal = Calendar.getInstance();
        secondDateCal.setTime(secondDate);
        return (firstDateCal.before(secondDateCal)); //{
        //if (firstDateCal.get(Calendar.DAY_OF_MONTH) < secondDateCal.get(Calendar.DAY_OF_MONTH)
        //      || firstDateCal.get(Calendar.MONTH) < secondDateCal.get(Calendar.MONTH)
        //    || firstDateCal.get(Calendar.YEAR) < secondDateCal.get(Calendar.YEAR)) {
        //return true;
        //}
        // return false;
    }

    /**
     * Converter from Date to Calendar
     *
     * @param date
     * @return a Calendar object with given date's parameters
     */
    public static Calendar dateToCalendar(Date date) {
        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(date);
        //System.out.println("");
        return dateCal;
    }

    /**
     *
     * @param date
     * @return the number of day from today to the given date
     */
    public static Byte upToDate(Date date) {
        ZoneId zone = ZoneId.of("Europe/London");
        LocalDate now = LocalDate.now(zone);
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());
        LocalDate eventDate = sqlDate.toLocalDate();

        Period period = now.until(eventDate);
        byte days = (byte) (period.getDays() + 1);

        if (days > WeatherManager.MAX_DAY_FORECAST) {
            return (byte) 0;
        } else {
            //System.out.println("" + days);
        }

        return (byte) days;
    }

    /**
     *
     * @param date
     * @return true if the given date is tomorrow
     */
    public static boolean isTomorrow(Date date) {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.DAY_OF_YEAR, t.get(Calendar.DAY_OF_YEAR) + 1);
        if (t.get(Calendar.DAY_OF_YEAR) == dateToCalendar(date).get(Calendar.DAY_OF_YEAR)) {
            return true;
        }
        /*
         if (t.getTimeInMillis() == date.getTime()) {
         return true;
         }*/
        return false;
    }

    public static boolean isThreeDaysBefore(Date date) {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.DAY_OF_YEAR, t.get(Calendar.DAY_OF_YEAR) + 3);
        if (t.get(Calendar.DAY_OF_YEAR) == dateToCalendar(date).get(Calendar.DAY_OF_YEAR)) {
            return true;
        }
        return false;
    }

    public static Date previousDay8Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 3);
        t.set(Calendar.HOUR, 8);
        return t.getTime();
    }

    public static Date previousDay11Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 3);
        t.set(Calendar.HOUR, 11);

        return t.getTime();
    }

    public static int eventPeriod(Date startDate, Date endDate) {
        java.sql.Date sqlStartDate = new java.sql.Date(startDate.getTime());
        java.sql.Date sqlEndDate = new java.sql.Date(endDate.getTime());
        int eventPeriod = Period.between(sqlStartDate.toLocalDate(), sqlEndDate.toLocalDate()).getDays() + 1;
        //System.out.println("Event PERIOD " + eventPeriod);
        return eventPeriod;
    }

    public static Date sumDayToDate(Date date, int days) {
        Calendar calDate = dateToCalendar(date);
        calDate.add(Calendar.DATE, days);
        return calDate.getTime();
    }

}
