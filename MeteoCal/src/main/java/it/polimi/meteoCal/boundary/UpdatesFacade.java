/*
 * Copyright (C) 2015 Michele Zanella <it.polimi.meteoCal>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.polimi.meteoCal.boundary;

import it.polimi.meteoCal.entity.Event;
import it.polimi.meteoCal.entity.Updates;
import it.polimi.meteoCal.entity.Users;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Michele Zanella <it.polimi.meteoCal>
 */
@Stateless
public class UpdatesFacade extends AbstractFacade<Updates> {

    @PersistenceContext
    EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UpdatesFacade() {
        super(Updates.class);
    }

    public List<Updates> findAllByUser(Users user) {
        List<Updates> updatesList = em.createNamedQuery(Updates.findAllByUser, Updates.class)
                .setParameter("user", user)
                .getResultList();
        return updatesList;
    }

    public int count(Users user) {
        List<Updates> updatesList = em.createQuery("SELECT up FROM UPDATES up WHERE up.user= :user  AND up.isRead=false")
                .setParameter("user", user)
                .getResultList();
        return updatesList.size();
    }

    public void removeAllByEvent(Event event) {
        em.createQuery("DELETE FROM UPDATES up WHERE up.event= :event")
                .setParameter("event", event)
                .executeUpdate();
    }

}
