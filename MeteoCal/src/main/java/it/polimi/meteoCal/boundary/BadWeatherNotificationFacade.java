/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.boundary;

import it.polimi.meteoCal.entity.BadWeatherNotification;
import it.polimi.meteoCal.entity.Event;
import it.polimi.meteoCal.entity.Users;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Michele
 */
@Stateless
public class BadWeatherNotificationFacade extends AbstractFacade<BadWeatherNotification> {

    @PersistenceContext
    EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BadWeatherNotificationFacade() {
        super(BadWeatherNotification.class);
    }

    public List<BadWeatherNotification> findAllByUser(Users user) {
        List<BadWeatherNotification> bwn = em.createNamedQuery(BadWeatherNotification.findAllByUser, BadWeatherNotification.class)
                .setParameter("user", user)
                .getResultList();
        return bwn;
    }

    public int count(Users user) {
        List<BadWeatherNotification> bwnList = em.createQuery("SELECT bwn FROM BADWEATHERNOTIFICATION bwn WHERE bwn.user= :user  AND bwn.isRead=false")
                .setParameter("user", user)
                .getResultList();
        return bwnList.size();
    }

    public void removeAllByEvent(Event event) {
        em.createQuery("DELETE FROM BADWEATHERNOTIFICATION bwn WHERE bwn.event= :event")
                .setParameter("event", event)
                .executeUpdate();
    }
}
