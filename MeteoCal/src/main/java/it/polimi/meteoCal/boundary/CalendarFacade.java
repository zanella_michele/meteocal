/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.boundary;

import it.polimi.meteoCal.entity.Calendar;
import it.polimi.meteoCal.entity.Event;
import it.polimi.meteoCal.entity.primeFaces.ScheduleModelImpl;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

/**
 *
 * @author Michele
 */
@Stateless
public class CalendarFacade extends AbstractFacade<Calendar> {

    @PersistenceContext
    EntityManager em;

    private final ScheduleModel eventModel = new ScheduleModelImpl(); //è questo che non si cancella con la fine della sessione...

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    /*
     public Event findEvent(ScheduleEventImpl event) {
     eventFacade
     /*
     List<Event> eventList = em.createNamedQuery(Event.findEvent, Event.class)
     .setParameter("id", event.getEventId())
     .getResultList();
     this.event = eventList.iterator().next();
     return this.event;
     }
     */

    /**
     *
     * @param calendar: the user's calendar to be stored in the database
     */
    @Override
    public void create(Calendar calendar) {
        em.persist(calendar);
    }

    /**
     *
     * @param calendar the calendar to be updated
     */
    @Override
    public void edit(Calendar calendar) {
        getEntityManager().merge(calendar);
    }

    /**
     *
     * @return the ScheduleModelImpl associated with the calendar
     */
    public ScheduleModel getEventModel() {
        return eventModel;
    }

    /**
     * This method is the observer method which is used to notify the creation
     * of an event
     *
     * @param event: the event created
     */
    public void notifyCreation(ScheduleEvent event) {
        eventModel.addEvent(event);
    }

    /**
     * This method is the observer method which is used to notify the update of
     * an event
     *
     * @param event the event updated
     */
    public void notifyUpdate(ScheduleEvent event) {
        eventModel.updateEvent(event);
    }

    /**
     * This method is the observer method which is used to notify that an event
     * is removed
     *
     * @param event the event to be deleted
     */
    public void notifyRemoving(ScheduleEvent event) {
        eventModel.deleteEvent(event);
    }

    /**
     * Constructor method
     */
    public CalendarFacade() {
        super(Calendar.class
        );
    }

    /**
     *
     * @param calendar the user's calendar
     * @return a list with the going events for the given user
     */
    public List<Event> getGoingEvents(Calendar calendar) {
        List<Event> goingEvents = em.createQuery("SELECT DISTINCT e FROM CALENDAR c JOIN EVENT e WHERE e = c.goingEvents AND c = :user")
                .setParameter("user", calendar)
                .getResultList();
        return goingEvents;
    }

}
