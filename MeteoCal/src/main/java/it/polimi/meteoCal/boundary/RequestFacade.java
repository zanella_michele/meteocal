/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.boundary;

import it.polimi.meteoCal.entity.Event;
import it.polimi.meteoCal.entity.Request;
import it.polimi.meteoCal.entity.Users;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Michele
 */
@Stateless
public class RequestFacade extends AbstractFacade<Request> {

    @PersistenceContext
    EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RequestFacade() {
        super(Request.class);
    }

    public List<Request> findAllByUser(Users user) {
        List<Request> requestsList = em.createNamedQuery(Request.findAllByUser, Request.class)
                .setParameter("user", user)
                .getResultList();
        return requestsList;
    }

    public int count(Users user) {

        List<Request> requestsList = em.createQuery("SELECT r FROM REQUEST r WHERE r.user= :user AND r.isRead=false")
                .setParameter("user", user)
                .getResultList();
        return requestsList.size();
    }

    public void removeAllByEvent(Event event) {
        em.createQuery("DELETE FROM REQUEST r WHERE r.event= :event")
                .setParameter("event", event)
                .executeUpdate();
    }

}
