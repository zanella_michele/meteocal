/*
 * Copyright (C) 2015 Michele Zanella <it.polimi.meteoCal>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.polimi.meteoCal.boundary;

import it.polimi.meteoCal.entity.Event;
import it.polimi.meteoCal.entity.Suggestion;
import it.polimi.meteoCal.entity.Users;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Michele Zanella <it.polimi.meteoCal>
 */
@Stateless
public class SuggestionFacade extends AbstractFacade<Suggestion> {

    @PersistenceContext
    EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SuggestionFacade() {
        super(Suggestion.class);
    }

    public List<Suggestion> findAllByUser(Users user) {
        List<Suggestion> suggestionsList = em.createNamedQuery(Suggestion.findAllByUser, Suggestion.class)
                .setParameter("user", user)
                .getResultList();
        return suggestionsList;
    }

    public int count(Users user) {
        List<Suggestion> suggestionsList = em.createQuery("SELECT s FROM SUGGESTION s WHERE s.user= :user  AND s.isRead=false")
                .setParameter("user", user)
                .getResultList();
        return suggestionsList.size();
    }

    public void removeAllByEvent(Event event) {
        em.createQuery("DELETE FROM SUGGESTION s WHERE s.event= :event")
                .setParameter("event", event)
                .executeUpdate();
    }

}
