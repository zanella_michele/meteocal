/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.boundary;

import it.polimi.meteoCal.entity.Calendar;
import it.polimi.meteoCal.entity.Event;
import it.polimi.meteoCal.entity.Request;
import it.polimi.meteoCal.entity.Users;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Michele
 */
@Stateless
public class EventFacade extends AbstractFacade<Event> {

    @PersistenceContext
    EntityManager em;
    @EJB
    WeatherForecastFacade weatherForecastFacade;
    @EJB
    RequestFacade requestFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Method to persist the event created
     *
     * @param event: the event created
     */
    @Override
    public void create(Event event) {
        em.persist(event);
    }

    /**
     * Method to update the event updated
     *
     * @param event the event updated
     */
    @Override
    public void edit(Event event) {
        /*  if (!oldForecastLists.isEmpty()) {
         for (WeatherForecast f : oldForecastLists) {
         weatherForecastFacade.remove(f);
         }
         }*/
        em.merge(event);
    }

    /**
     * Method to remove the event selected
     *
     * @param event the event to be removed
     */
    @Override
    public void remove(Event event) {
        em.remove(em.merge(event));
    }

    /**
     * Method to find the event selected
     *
     * @param id the event's id
     * @return the event found or null if it's not found
     */
    public Event find(String id) {
        return em.find(Event.class, id);
    }

    public List<Users> getInvitedUser(Event event) {
        List<Request> requestsList = em.createNamedQuery(Request.findInvited, Request.class)
                .setParameter("event", event)
                .getResultList();
        List<Users> invitedUsers = new ArrayList<>();
        for (Request r : requestsList) {
            invitedUsers.add(r.getUser());
        }
        return invitedUsers;
    }

    public List<Users> getRequestedUser(Event event) {
        List<Request> requestsList = em.createNamedQuery(Request.findRequested, Request.class)
                .setParameter("event", event)
                .getResultList();
        List<Users> invitedUsers = new ArrayList<>();
        for (Request r : requestsList) {
            invitedUsers.add(r.getUser());
        }
        return invitedUsers;
    }

    public List<Calendar> getGoingUsers(Event event) {
        List<Calendar> goingUsers = em.createQuery("SELECT DISTINCT c FROM EVENT e JOIN CALENDAR c "
                + "WHERE c=e.goingUsers AND e= :event")
                .setParameter("event", event)
                .getResultList();
        return goingUsers;
    }

    /**
     * Constructor method
     */
    public EventFacade() {
        super(Event.class);
    }

}
