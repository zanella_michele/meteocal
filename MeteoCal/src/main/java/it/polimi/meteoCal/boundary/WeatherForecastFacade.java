/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.polimi.meteoCal.boundary;

import it.polimi.meteoCal.entity.Event;
import it.polimi.meteoCal.entity.WeatherForecast;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Michele
 */
@Stateless
public class WeatherForecastFacade extends AbstractFacade<WeatherForecast> {

    @PersistenceContext
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public WeatherForecastFacade() {
        super(WeatherForecast.class);
    }

    @Override
    public void remove(WeatherForecast forecast) {
        em.remove(em.merge(forecast));
    }

    public void remove(List<WeatherForecast> forecastList) {
        for (WeatherForecast forecast : forecastList) {
            this.remove(forecast);
        }
    }

    public void removeAllByEvent(Event event) {
        em.createNamedQuery(WeatherForecast.removeAllByEvent, WeatherForecast.class)
                .setParameter("event", event)
                .executeUpdate();
    }
}
