# MeteoCal #

MeteoCal is a weather-aware social calendar development by Michele Zanella and Andrea Sorbelli as a project for the Software Engineering 2 course of Politecnico of Milano. It is a client-server system that allows the signed-in user to plan some events in a calendar-based interface. The events support sharing and invitation of other registered user in the system. The most important feature is the possibility to enable a preferred weather condition of the event, so that the system, verifying the weather forecast, can send an alert notification to the invited people in case of bad weather condition and can send a rescheduling proposed date to the event's owner. 
### How do I get set up? ###

Here you can find the Installation Guide: https://www.dropbox.com/s/iyzaoaqxrs8aymc/InstallationGuide.pdf?dl=0

### Doc ###

Main documentation:

* RASD document: https://www.dropbox.com/s/v0f4bjbi1n3r4th/RASDrev2.pdf?dl=0

* Design document: https://www.dropbox.com/s/im8e7zrq4gv2o2n/DDrev1.pdf?dl=0

* User Guide: https://www.dropbox.com/s/d49peawkvrpsvam/UserGuide.pdf?dl=0